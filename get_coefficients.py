def super_magic_multiplication(x, y: int ) -> int:
    """ :param x, y (int)
    :return: Int.: x*y"""
    return x * y
